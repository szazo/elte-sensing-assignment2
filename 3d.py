import argparse
import open3d as o3d

# Parse the arguments
parser = argparse.ArgumentParser(
    prog="PointCloud Poisson reconstruction",
    description="Converts point cloud to 3d triangle mesh."
)
parser.add_argument("input", help="The input point cloud filepath (.ply)")
parser.add_argument(
    "output_prefix", help="The output prefix for images, clouds to be exported")
parser.add_argument(
    "--gui", action=argparse.BooleanOptionalAction, default=True)

args = parser.parse_args()
input_filepath = args.input
output_prefix = args.output_prefix
show_gui = args.gui

close_render_params = dict(
    zoom=0.65,
    front=[-0.078275461620454484, -
           0.048750089385130632, 0.99573911286694305],
    up=[0, -1, 0],
    lookat=[58.499428571428638, -5.3192023809523619, -2652.7550000000001])

distant_render_params = dict(
    zoom=0.89,
    front=[-0.078275461620454484, -
           0.048750089385130632, 0.99573911286694305],
    up=[0, -1, 0],
    lookat=[58.499428571428638, -5.3192023809523619, -2652.7550000000001])


def visualize(geometries, render_params: dict, out_filepath=None,
              show_gui=True, point_show_normals=False,
              mesh_show_back_face=False, mesh_show_wireframe=False):
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.set_full_screen(True)

    for geometry in geometries:
        vis.add_geometry(geometry)

    # camera params
    ctr = vis.get_view_control()
    ctr.set_lookat(render_params['lookat'])
    ctr.set_up(render_params['up'])
    ctr.set_front(render_params['front'])
    ctr.set_zoom(render_params['zoom'])

    # render options
    opt = vis.get_render_option()
    opt.point_show_normal = point_show_normals
    opt.mesh_show_back_face = mesh_show_back_face
    opt.mesh_show_wireframe = mesh_show_wireframe

    # update
    vis.poll_events()
    vis.update_renderer()

    # img = vis.capture_screen_float_buffer(True)
    # plt.imshow(img)
    # plt.savefig('foo2.png')

    # capture
    if out_filepath is not None:
        vis.capture_screen_image(out_filepath)

    # show gui
    if show_gui:
        vis.run()

    # cleanup
    vis.destroy_window()


pcd = o3d.io.read_point_cloud(input_filepath)
print('Original cloud size: ', len(pcd.points))

# Show the original point cloud
visualize([pcd], close_render_params,
          show_gui=show_gui, out_filepath=f'{output_prefix}_pcd.png')

# Downsample
voxel_size = 7.
downsampled_pcd = pcd.voxel_down_sample(voxel_size=voxel_size)
print(f"Cloud size after downsample (with voxel size {voxel_size}): ", len(
    downsampled_pcd.points))

# Outlier removal
filtered_pcd, index_mask = downsampled_pcd.remove_statistical_outlier(
    nb_neighbors=20,
    std_ratio=0.5)


# Show filtered point cloud
visualize([filtered_pcd], distant_render_params,
          show_gui=show_gui, out_filepath=f'{output_prefix}_pcd_filtered.png')

print('Estimating normals...')
filtered_pcd.estimate_normals()
filtered_pcd.orient_normals_to_align_with_direction([0.0, -1.0, 0.0])

o3d.io.write_point_cloud(
    f'{output_prefix}_pcd_normals.ply', filtered_pcd, write_ascii=True)

# Visualize normals
visualize([filtered_pcd], distant_render_params,
          show_gui=show_gui, out_filepath=f'{output_prefix}_pcd_normals.png',
          point_show_normals=True)
