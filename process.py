import os
import shutil
import itertools
import subprocess
import pandas as pd

input_dirs = ['Aloe', 'Art', 'Books', 'Dolls',
              'Flowerpots', 'Lampshade1', 'Laundry', 'Midd1', 'Moebius',
              'Monopoly', 'Reindeer', 'Wood1']

def read_dmin(directory):
    with open(f'data/{directory}/dmin.txt', 'r') as file:
        content = file.read().strip()
        print('content', content)
        print('number', int(content))
        return int(content)


input_dmins = [read_dmin(directory) for directory in input_dirs]
print('dmins2', input_dmins)

baseline_mm = 160
focal_length_px = 3740

out_dir = 'output'
out_upload_dir = 'output_upload'

os.makedirs(out_dir, exist_ok=True)
os.makedirs(out_upload_dir, exist_ok=True)
inputs = [[
    d,
    f'data/{d}/disp1.png',
    f'data/{d}/view1.png',
    f'{out_dir}/{d}',
    f'{out_upload_dir}/{d}',
] for d in input_dirs]

# append dmins
list(map(list.append, inputs, input_dmins))

window_sizes = [5, 7, 13]
gaussian_ks = [1.5, 2.5, 3.5]
range_sigmas = [20, 50, 100]

#  hyperparameter combinations
hyperparams = {
    'b': {
        'window_size': window_sizes,
        'gaussian_k': gaussian_ks,
        'range_sigma': range_sigmas
    },
    'jb': {
        'window_size': window_sizes,
        'gaussian_k': gaussian_ks,
        'range_sigma': range_sigmas
    },
    'jbu': {
        'window_size': window_sizes,
        'gaussian_k': gaussian_ks,
        'range_sigma': range_sigmas
    },
    'ijbu': {
        'window_size': window_sizes,
        'gaussian_k': gaussian_ks,
        'range_sigma': range_sigmas
    },
}

print(inputs)

noise_standard_deviation = 20
downsampling_factor = 4

# these combinations will be used for 3d reconstruction
# (algo,window_size,gaussian_k,range_sigma)
threed_combinations = [('b', 13, 1.5, 50),
                       ('jb', 13, 1.5, 20),
                       ('jbu', 5, 2.5, 20),
                       ('ijbu', 5, 3.5, 20)]

# this will contain the result
experiments_df = pd.DataFrame()

for name, image, guiding_image, output, output_upload, dmin in inputs:
    print(name, image, guiding_image, output, dmin)

    for algo, params in hyperparams.items():

        window_sizes = params['window_size']
        gaussian_ks = params['gaussian_k']
        range_sigmas = params['range_sigma']

        param_product = list(itertools.product(
            window_sizes, gaussian_ks, range_sigmas))

        for window_size, gaussian_k, range_sigma in param_product:
            print(
                f'processing "{name}" with algorithm "{algo}" ' +
                f'(window_size={window_size}, gaussian_k={gaussian_k},' +
                f' range_sigma={range_sigma})')

            suffix = f'_{algo}_{window_size}_{gaussian_k}_{range_sigma}'
            output_with_suffix = output + suffix
            output_upload_with_suffix = output_upload + suffix

            has_3d = len([x for x in threed_combinations if x[0] ==
                          algo and x[1] == window_size and x[2] == gaussian_k
                          and x[3] == range_sigma]) > 0
            # execute the algorithm
            process = subprocess.run(['build/assignment2_filters',
                                      '--windowsize', str(window_size),
                                      '--gaussian-k', str(gaussian_k),
                                      '--range-sigma', str(range_sigma),
                                      '--focallength', str(focal_length_px),
                                      '--baseline', str(baseline_mm),
                                      '--dmin', str(dmin),
                                      '--nogui',
                                      f'--point-cloud={has_3d}',
                                      '--guiding-image', guiding_image,
                                      algo,
                                      image,
                                      output_with_suffix
                                      ],
                                     universal_newlines=True)

            if process.returncode != 0:
                exit(1)

            # 3d normal estimation
            if has_3d:
                process = subprocess.run(['python3',
                                          '3d.py',
                                          '--no-gui',
                                          f'{output_with_suffix}.ply',
                                          output_with_suffix],
                                         universal_newlines=True)
                if process.returncode != 0:
                    exit(1)

                to_copy_suffix = ['output', 'difference', 'obfuscated', 'pcd', 'pcd_filtered', 'pcd_normals']

                shutil.copy(image, f'{output_upload_with_suffix}_original.png')
                for suffix in to_copy_suffix:
                    shutil.copy(f'{output_with_suffix}_{suffix}.png', f'{output_upload_with_suffix}_{suffix}.png')

            # load the stat
            df = pd.read_csv(output_with_suffix + "_stat.csv")
            df.insert(0, 'experiment', name, True)
            df.insert(1, 'algorithm', algo, True)
            df.insert(2, 'window_size', window_size)
            df.insert(3, 'gaussian_k', gaussian_k)
            df.insert(4, 'gaussian_sigma', (window_size // 2) / gaussian_k)
            df.insert(5, 'range_sigma', range_sigma)
            df.insert(6, 'has_3d', has_3d)

            # add to the list
            experiments_df = pd.concat([experiments_df, df])
            experiments_df.to_csv(f'{out_upload_dir}/experiments.csv', index=False)
