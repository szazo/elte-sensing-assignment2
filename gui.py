import io
import os
import PySimpleGUI as sg
from PIL import Image
import subprocess

print(sg.theme_list())
sg.theme('LightGreen')

algorithms = ['Bilateral', 'Joint Bilateral',
              'Joint Bilateral Upsampling', 'Iterative Joint Bilateral Upsampling']
algorithm_ids = ['b', 'jb', 'jbu', 'ijbu']

algorithms_radio = [sg.Radio(algo, '-ALGORITHM-', default=index == 0,
                             key=f'-ALGORITHM_{algo}-') for index, algo in enumerate(algorithms)]
print(algorithms_radio)


def parse_algo(values):
    for index, algo in enumerate(algorithms):
        if values[f'-ALGORITHM_{algo}-']:
            return algorithm_ids[index]


left_column = [
    [sg.Text('Input image:'), sg.Input('data/Aloe/disp1.png',
                                       key='-INPUTFILE-', enable_events=True), sg.FileBrowse()],
    [sg.Text('Guiding image:'), sg.Input('data/Aloe/view1.png',
                                         key='-GUIDINGFILE-', enable_events=True), sg.FileBrowse()],
    [sg.HorizontalSeparator()],
    [sg.Text('Noise std.:'), sg.Slider(default_value=20,
                                       key='-NOISE_STANDARD_DEVIATION-', range=(3, 100), orientation='horizontal')],
    [sg.Text('Downsample factor:'), sg.Slider(default_value=4,
                                              key='-DOWNSAMPLING_FACTOR-', range=(2, 10), orientation='horizontal')],
    [sg.HorizontalSeparator()],
    [sg.Text('Algorithm:')],
    algorithms_radio,
    [sg.Text('Window size:'), sg.Slider(key='-WINDOW_SIZE-', range=(3, 29),
                                        size=(50, 10), orientation='horizontal', tick_interval=2, resolution=2)],
    [sg.Text('Gaussian k (sigma=(window_size // 2) / k):'),
     sg.Input('2.5', key='-GAUSSIAN_K-')],
    [sg.Text('Range sigma'), sg.Input('20', key='-RANGE_SIGMA-')],
    [sg.Text('3D'), sg.Checkbox(
        'Generate and display 3D point cloud and normals', key='-3D-', default=True)],
    [sg.Button('Run'), sg.Button('Exit')]
]

image_size = (800, 900)

# https://www.blog.pythonlibrary.org/2022/01/25/pysimplegui-an-intro-to-laying-out-elements/
# https://stackoverflow.com/questions/70892428/pysimplegui-how-do-i-convert-a-value-from-string-to-float-using
right_column = [
    [sg.Text('Source image:')],
    [sg.Image(key="-IMAGE-", expand_x=True)],
    [sg.Text('Guiding image:')],
    [sg.Image(key="-GUIDINGIMAGE-", size=image_size,
              expand_x=True, expand_y=True)],
    [sg.Text('Obfuscated image:')],
    [sg.Image(key="-OBFUSCATEDIMAGE-", size=image_size,
              expand_x=True, expand_y=True)],
    [sg.Text('Filtered image:')],
    [sg.Image(key="-FILTEREDIMAGE-", size=image_size,
              expand_x=True, expand_y=True)],
    [sg.Text('Difference image:')],
    [sg.Image(key="-DIFFERENCEIMAGE-", size=image_size,
              expand_x=True, expand_y=True)],
]

layout = [
    [sg.Column(left_column),
     sg.VSeparator(),
     sg.Column(right_column, key='-RIGHTCOLUMN-', scrollable=True,
               vertical_scroll_only=True, vertical_alignment='top',
               expand_x=True, expand_y=True)],
]

# create the window
window = sg.Window('Filtering and Upsampling', layout, resizable=True)


def load_image(filename, target_id, size=image_size):
    image = Image.open(filename)
    image.thumbnail(size)
    bytes_io = io.BytesIO()
    image.save(bytes_io, format='PNG')
    window[target_id].update(data=bytes_io.getvalue())


def process(image_path, guiding_image_path, algo, output_prefix, window_size,
            gaussian_k, range_sigma,
            noise_standard_deviation, downsampling_factor, point_cloud=True):
    baseline_mm = 160
    focal_length_px = 3740
    dmin = 200

    process = subprocess.run(['build/assignment2_filters',
                              '--windowsize', str(window_size),
                              '--gaussian-k', str(gaussian_k),
                              '--range-sigma', str(range_sigma),
                              '--noise-standard-deviation', str(
                                  noise_standard_deviation),
                              '--downsampling-factor', str(
                                  downsampling_factor),
                              '--focallength', str(focal_length_px),
                              '--baseline', str(baseline_mm),
                              '--dmin', str(dmin),
                              '--nogui',
                              f'--point-cloud={point_cloud}',
                              '--guiding-image', guiding_image_path,
                              algo,
                              image_path,
                              output_prefix
                              ],
                             universal_newlines=True)

    return process.returncode == 0


def show_3d(input_ply, output_prefix):
    process = subprocess.run(['python3', '3d.py',
                              input_ply,
                              output_prefix],
                             universal_newlines=True)

    return process.returncode == 0


# start the event loop
while True:
    # get event and values of input
    event, values = window.read()
    print('event', event)
    if event == '-INPUTFILE-':
        filename = values['-INPUTFILE-']
        try:
            load_image(filename, '-IMAGE-')
        except:
            pass
    elif event == '-GUIDINGFILE-':
        filename = values['-GUIDINGFILE-']
        try:
            load_image(filename, '-GUIDINGIMAGE-')
        except:
            pass
    elif event == 'Run':

        # collect the parameters
        print(values)
        algo = parse_algo(values)
        image_path = values['-INPUTFILE-']
        guiding_path = values['-GUIDINGFILE-']
        window_size = int(values['-WINDOW_SIZE-'])
        gaussian_k = values['-GAUSSIAN_K-']
        range_sigma = values['-RANGE_SIGMA-']
        noise_standard_deviation = int(values['-NOISE_STANDARD_DEVIATION-'])
        downsampling_factor = values['-DOWNSAMPLING_FACTOR-']
        point_cloud = bool(values['-3D-'])

        out_dir = 'output'
        os.makedirs(out_dir, exist_ok=True)
        output_prefix = f'{out_dir}/gui'
        success = process(image_path, guiding_path, algo, output_prefix,
                          window_size, gaussian_k, range_sigma,
                          noise_standard_deviation, downsampling_factor,
                          point_cloud)
        if success:
            load_image(f'{output_prefix}_obfuscated.png', '-OBFUSCATEDIMAGE-')
            load_image(f'{output_prefix}_output.png', '-FILTEREDIMAGE-')
            load_image(f'{output_prefix}_difference.png', '-DIFFERENCEIMAGE-')
            window['-RIGHTCOLUMN-'].contents_changed()
            if point_cloud:
                show_3d(f'{output_prefix}.ply', output_prefix)
    elif event == sg.WINDOW_CLOSED or event == 'Exit':
        break

window.close()
