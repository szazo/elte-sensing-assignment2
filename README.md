# Filtering and Upsampling

Filtering and Upsampling using several algorithms.

More information and results can be found here: https://colab.research.google.com/drive/1eyDAI5O8q4xmqFPQtIGA9_QK5m22fts_?usp=sharing

## Getting started

To build and run the experiments, use the following commands

Build:
```
./compile.sh
```

To run the experiments:

```
python3 process.py
```

To run the gui to interactively test:

```
python3 gui.py
```


## Dataset

Part of the following datasets was used for the experiments:
  * https://vision.middlebury.edu/stereo/data/scenes2005/
  * https://vision.middlebury.edu/stereo/data/scenes2006/
