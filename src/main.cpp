#include "cxxopts.hpp"
#include <array>
#include <cmath>
#include <fstream>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/core/hal/interface.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/core/matx.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/quality.hpp>
#include <ostream>
#include <happly.h>
#include <vector>

enum class Filter { b, jb, jbu, ijbu };

bool is_guided_filter(Filter f) {
  return f == Filter::jb || f == Filter::jbu ||
     f == Filter::ijbu;
}

bool is_upsampling_filter(Filter f) {
  return f == Filter::jbu || f == Filter::ijbu;
}

inline int gaussian_half_window_size(const int &window_size) {
  // only symmetric, if int, there is no double entry at the center
  return window_size / 2;
}

double calculate_gaussian_sigma_from_k(const int &window_size, const double &k) {
  //  const double sigma = std::sqrt(2) * gaussian_half_window_size(window_size) / k;
  const double sigma = gaussian_half_window_size(window_size) / k;  
  return sigma;
}

void create_gaussian_kernel(const int &window_size, const double &k, cv::Mat &mask) {

  cv::Size mask_size(window_size, window_size);
  mask = cv::Mat(mask_size, CV_8UC1);
  int sum_mask = 0;

  const int half_window_size = gaussian_half_window_size(window_size);
  const double sigma = calculate_gaussian_sigma_from_k(window_size, k);
  const double sigma_squared = std::pow(sigma, 2);

  // iterate over the window and fill gaussian values
  for (int r = 0; r < window_size; ++r) {
    for (int c = 0; c < window_size; ++c) {
      double r2 = std::pow(r - half_window_size, 2) + std::pow(c - half_window_size, 2);

      uchar mask_value = 255 * std::exp(-r2 / (2 * sigma_squared));
      mask.at<uchar>(r, c) = mask_value;
      sum_mask += mask_value;
    }
  }
}

void create_exponential_bilateral_range_kernel(const int &window_size, const double &range_sigma,
                                               double (&range_mask)[256]) {

  const double range_sigma_squared = std::pow(range_sigma, 2);
  
  for (int diff = 0; diff < 256; ++diff) {
    range_mask[diff] = std::exp(-std::pow(diff, 2) / (2 * range_sigma_squared));
  }
}

double calculate_bilateral_weight(const cv::Mat &input,
                                const int &window_size,
                                const int &center_row, const int &center_col,
                                const int &window_row, const int &window_col,
                                const cv::Mat &kernel,
                                const double (&weightning_range_kernel)[256]) {

  // get the intensity at the center, we will calculate difference from this at each point in the window
  int intensity_center = static_cast<int>(input.at<uchar>(center_row, center_col));

  // get the intensity at the current window position
  int intensity = static_cast<int>(input.at<uchar>(center_row + window_row, center_col + window_col));

  // compute the difference between the pixel and the center pixel
  int diff = std::abs(intensity_center - intensity);

  // compute the range kernel's value (it will be between [0.0..1.0])
  // NOTE: for the case of exponential range kernel, larger difference means that spatial
  // filter won't be used as much.
  double weight_range = weightning_range_kernel[diff];

  // calculate the spatial weight (it will be between [0..255])
  int weight_spatial = static_cast<int>(kernel.at<uchar>(window_row + window_size / 2,
                                                         window_col + window_size / 2));

  // weight the spatial kernel value using the range weight (it will be between [0.0..255.0])
  double weight = weight_range * weight_spatial;

  return weight;
}

void joint_bilateral_convolution(const cv::Mat &input,
                                 const cv::Mat &guiding_image,
                                 const int &window_size,
                                 const cv::Mat &kernel,
                                 const double (&weightning_range_kernel)[256],
                                 cv::Mat &output
                                 ) {

  const auto rows = input.rows;
  const auto cols = input.cols;
  output = cv::Mat::zeros(input.size(), input.type());

  for (int r = window_size / 2; r < rows - window_size / 2; ++r) {
    for (int c = window_size / 2; c < cols - window_size / 2; ++c) {

      // the sum of convolved intensities
      double sum = 0;

      // the sum of the weights for normalization
      double sum_weights = 0;
      
      // iterate over the window
      for (int i = -window_size / 2; i <= window_size / 2; ++i) {
        for (int j = -window_size / 2; j <= window_size / 2; ++j) {

          // calculate the bilateral weight for the guiding image
          double weight = calculate_bilateral_weight(guiding_image,
                                                     window_size, r, c, i, j, kernel, weightning_range_kernel);
          
          // convolve the input image using the calculated guiding weight
          int intensity = static_cast<int>(input.at<uchar>(r + i, c + j));
          sum += weight * intensity;

          // add weight for normalization
          sum_weights += weight;
        }
      }

      // store the normalized intensity value
      const double normalized_value = sum / sum_weights;

      // cast and store
      output.at<uchar>(r,c) = static_cast<uchar>(normalized_value);
    }
  }
}

void iterative_joint_bilateral_upsampling_convolution(const cv::Mat &lowres_input,
                                            const cv::Mat &guiding_image,
                                            const int &window_size,
                                            const cv::Mat &kernel,
                                            const double (&weightning_range_kernel)[256],
                                            cv::Mat &output) {

  std::cout << "loweres" << lowres_input.size() << std::endl;

  // calculate the number of required upsampling when doubling the size at each iteration (log2)
  int upsample_count = std::log2(guiding_image.size().width / static_cast<double>(lowres_input.size().width));

  // copy the original downsampled image to the working copy
  cv::Mat current_lowres;
  lowres_input.copyTo(current_lowres);

  // iterate
  for (int i = 1; i <= (upsample_count - 1); i++) {
    // increase the current lowres image by factor of 2
    cv::Size2i new_lowres_size = current_lowres.size() * 2;
    cv::Mat new_lowres;
    std::cout << "new lowres size: " << new_lowres_size << std::endl;
    cv::resize(current_lowres, new_lowres, new_lowres_size, 0, 0, cv::INTER_LINEAR);
    current_lowres = new_lowres;
    std::cout << "new lowres size2: " << current_lowres.size() << std::endl;
    
    // resize the guiding image to match the size of the current lowres image
    cv::Mat current_guiding_image;
    cv::resize(guiding_image, current_guiding_image, current_lowres.size(), 0, 0, cv::INTER_LINEAR);

    // joint bilateral filter using the current lower resolution guiding image
    cv::Mat filtered_lowres;
    joint_bilateral_convolution(current_lowres,
                                current_guiding_image,
                                window_size, kernel, weightning_range_kernel, filtered_lowres);
    current_lowres = filtered_lowres;
  }

  // resize to the final size and do the guided filtering
  cv::Mat final_lowres;
  cv::resize(current_lowres, final_lowres, guiding_image.size(), 0, 0, cv::INTER_LINEAR);
  joint_bilateral_convolution(final_lowres,
                              guiding_image,
                              window_size, kernel, weightning_range_kernel, output);
}

void joint_bilateral_upsampling_convolution(const cv::Mat &lowres_input,
                                            const cv::Mat &guiding_image,
                                            const int &window_size,
                                            const cv::Mat &kernel,
                                            const double (&weightning_range_kernel)[256],
                                            cv::Mat &output) {


  // calculate the upsample factor
  const double factor = guiding_image.size().width / static_cast<double>(lowres_input.size().width);

  std::cout << "Factor" << factor << std::endl;

  // iterate over the guiding image's range
  const auto rows = guiding_image.rows;
  const auto cols = guiding_image.cols;
  output = cv::Mat::zeros(guiding_image.size(), guiding_image.type());

  std::cout << "rows: " << rows << " cols: " << cols << " size: " << guiding_image.size() << std::endl;

  for (int r = window_size / 2; r < rows - window_size / 2; ++r) {
    for (int c = window_size / 2; c < cols - window_size / 2; ++c) {

      // the sum of convolved intensities
      double sum = 0;

      // the sum of the weights for normalization
      double sum_weights = 0;

      // iterate over the window
      for (int i = -window_size / 2; i <= window_size / 2; ++i) {
        for (int j = -window_size / 2; j <= window_size / 2; ++j) {

          // calculate the coordinate in the downsampled image
          const int downsampled_row = (r + i) / factor;
          const int downsampled_col = (c + j) / factor;

          if (c+j > 1388 && r+i == 10) {
            std::cout << "source: " << r+i << ";" << c + j << " -> " << downsampled_row << ";" << downsampled_col << std::endl;
            
          }

          // calculate the bilateral weight for the guiding image
          double weight = calculate_bilateral_weight(guiding_image,
                                                     window_size, r, c, i, j, kernel, weightning_range_kernel);
          
          // // convolve the input downsampled image using the calculated guiding weight
          int intensity = static_cast<int>(lowres_input.at<uchar>(downsampled_row, downsampled_col));
          sum += weight * intensity;

          // add weight for normalization
          sum_weights += weight;
        }
      }

      // store the normalized intensity value
      const double normalized_value = sum / sum_weights;

      // cast and store
      output.at<uchar>(r,c) = static_cast<uchar>(normalized_value);
    }
  }
}

void bilateral_convolution(const cv::Mat &input, const int &window_size,
                           const cv::Mat &kernel,
                           const double (&weightning_range_kernel)[256],
                           cv::Mat &output) {

  const auto rows = input.rows;
  const auto cols = input.cols;
  output = cv::Mat::zeros(input.size(), input.type());

  for (int r = window_size / 2; r < rows - window_size / 2; ++r) {
    for (int c = window_size / 2; c < cols - window_size / 2; ++c) {

      // get the intensity at the center, we will calculate difference from this at each point in the window
      //int intensity_center = static_cast<int>(input.at<uchar>(r, c));

      // the sum of convolved intensities
      double sum = 0;

      // the sum of the weights for normalization
      double sum_weights = 0;
      
      // iterate over the window
      for (int i = -window_size / 2; i <= window_size / 2; ++i) {
        for (int j = -window_size / 2; j <= window_size / 2; ++j) {
          double weight = calculate_bilateral_weight(input, window_size, r, c, i, j, kernel, weightning_range_kernel);
          int intensity = static_cast<int>(input.at<uchar>(r + i, c + j));
          
          // convolve
          sum += weight * intensity;

          // add weight for normalization
          sum_weights += weight;
        }
      }

      // store the normalized intensity value
      const double normalized_value = sum / sum_weights;

      // cast and store
      output.at<uchar>(r,c) = static_cast<uchar>(normalized_value);
    }
  }
}

using Point3 = std::array<double, 3>;
using Color3 = std::array<double, 3>;

void disparity_map_to_point_cloud(const cv::Mat &disparity_image, const cv::Mat &color_image,
                                  const int &window_size,
                                  const double &baseline, const double &focal_length, const double &dmin,
                                  const std::string &output_file_prefix) {

  const double &b = baseline;
  const double &f = focal_length;

  const int &rows = disparity_image.rows;
  const int &cols = disparity_image.cols;

  happly::PLYData ply_out;
  
  std::string output_ply_filepath = output_file_prefix + ".ply";

  std::vector<Point3> vertex_positions;
  std::vector<Point3> vertex_colors;
  
  for (int r = window_size / 2; r < rows - window_size / 2; r++) {

    std::cout << "Reconstructing 3D point cloud from disparities... "
              << std::ceil(
                     ((r) / static_cast<double>(rows + 1)) *
                     100)
              << "%\r" << std::flush;

    for (int c = window_size / 2; c < cols - window_size / 2; c++) {

      // get the disparity with the added dmin
      const double d = static_cast<double>(disparity_image.at<uchar>(r, c)) + dmin;

      // calculate the horizontal, camera centered image plane coordinates using the disparity
      const double u1 = c - (cols / 2.0);
      const double u2 = u1 - d;

      // calculate vertical camera centered image plane coordinate
      const double v = r - (rows / 2.0);

      // calculate the depth
      double Z = (baseline * focal_length) / d; // mm * px / px = mm
      Z = -Z; // to be the same orientation as the others
      
      const double X = -(baseline * (u1 + u2)) / (2 * d); // mm * px / px = mm
      const double Y = (baseline * v) / d; // mm * px / px = mm

      vertex_positions.push_back(Point3({X, Y, Z}));

      if (color_image.size().width > 0) {
        // if non empty, we write the color too
        cv::Vec3b color = color_image.at<cv::Vec3b>(r, c);
        const cv::Vec3d normalized_color = (1.0 / 255) * cv::Vec3d(color);
        const Point3 colored = Point3({normalized_color[2], normalized_color[1], normalized_color[0]});
        vertex_colors.push_back(colored);
      }
    }
  }

  ply_out.addVertexPositions(vertex_positions);
  if (vertex_colors.size() > 0) {
    ply_out.addVertexColors(vertex_colors);
  }
  ply_out.write(output_ply_filepath, happly::DataFormat::Binary);
}

// add image noise with zero mean and specified standard deviation (default is 25)
void add_image_noise(const cv::Mat &image, const uchar &standard_deviation) {
  // generate the noise matrix
  cv::Mat noise(image.size(), image.type());
  const uchar mean = 0;
  cv::randn(noise, mean, standard_deviation);
  
  // add to the image (NOTE: it can overflow, maybe clamp then update would be better)
  image += noise;
}

bool parse_filter(const std::string &filter_name, Filter &filter) {
  if (filter_name == "b") {
    filter = Filter::b;
  } else if (filter_name == "jb") {
    filter = Filter::jb;
  } else if (filter_name == "jbu") {
    filter = Filter::jbu;
  } else if (filter_name == "ijbu") {
    filter = Filter::ijbu;
  } else {
    return false;
  }

  return true;
}

// evaluate the disparity map using ground truth
void evaluate(const cv::Mat &disparities_est, const cv::Mat &disparities_gt,
              cv::Mat &difference_image, std::string &stat_filename,
              const double &elapsed_time) {

  if (disparities_est.rows != disparities_gt.rows ||
      disparities_est.cols != disparities_gt.cols) {
    std::cerr << "ERROR: Ground truth disparity image size mismatch, exiting: "
              << disparities_gt.rows << ", " << disparities_gt.cols
              << std::endl;
    exit(1);
  }

  // create the difference image
  cv::absdiff(disparities_est, disparities_gt, difference_image);

  // several metrics
  cv::Mat squared_difference;
  cv::pow(difference_image, 2, squared_difference);

  const double n = disparities_est.rows * disparities_est.cols;
  const double ssd = cv::sum(squared_difference)[0];
  const double mse = ssd / n;
  const double rmse = std::sqrt(mse);

  const double sad = cv::sum(difference_image)[0];
  const double mad = sad / n;

  // SSIM
  cv::Mat ssid_quality_map;
  double ssim = cv::quality::QualitySSIM::compute(
      disparities_gt, disparities_est, ssid_quality_map)[0];

  // PSNR
  cv::Mat psnr_quality_map;
  double psnr = cv::quality::QualityPSNR::compute(
      disparities_gt, disparities_est, psnr_quality_map)[0];

  // write into the stat file
  std::ofstream stat_file;
  stat_file.open(stat_filename);

  stat_file << "ssd,mse,rmse,sad,mad,ssim,psnr,time\n";
  stat_file << ssd << "," << mse << "," << rmse << "," << sad << "," << mad
            << "," << ssim << "," << psnr << "," << elapsed_time << "\n";
  stat_file.close();
}

// https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
inline std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch (depth) {
  case CV_8U:
    r = "8U";
    break;
  case CV_8S:
    r = "8S";
    break;
  case CV_16U:
    r = "16U";
    break;
  case CV_16S:
    r = "16S";
    break;
  case CV_32S:
    r = "32S";
    break;
  case CV_32F:
    r = "32F";
    break;
  case CV_64F:
    r = "64F";
    break;
  default:
    r = "User";
    break;
  }

  r += "C";
  r += (chans + '0');

  return r;
}

int main(int argc, char **argv) {

  cxxopts::Options options("Filters", "Sensing Assignment 2 for Bilateral and Joint Filtering/Upsampling");

  const std::string possible_filter_names = "'b', 'bm', 'jb', 'jbm', 'jbu', 'jbmu', 'ijbu', 'ijbmu'";
  
  options.add_options()
    ("image", "The input image", cxxopts::value<std::string>())
    ("output", "The name prefix for the output", cxxopts::value<std::string>())
    ("filter", "Filter/method to use; possible values: " + possible_filter_names, cxxopts::value<std::string>())
    ("g,guiding-image", "The path for guiding image in case of guided filters", cxxopts::value<std::string>())
    ("w,windowsize", "The window size used in all of the algorithms. It should be odd.", cxxopts::value<int>()->default_value("7"))
    ("k,gaussian-k", "The k value used to calculate sigma for Gaussian filter: sigma = (window_size / 2) / k.", cxxopts::value<double>()->default_value("2.5"))
    ("r,range-sigma", "The sigma for the range kernel. The range kernel is used to weighten the main kernel using a gaussian function of the norm of intensity difference between the window center and the current window coordinate.", cxxopts::value<double>()->default_value("25"))
    ("f,focallength", "The focal length in pixels for disparity 3D reconstruction", cxxopts::value<double>()->default_value("3740"))
    ("b,baseline", "The baseline of the two cameras in mm", cxxopts::value<double>()->default_value("160"))
    ("d,dmin", "To map the disparities into 3D coordinates, add this value to each disparity value.", cxxopts::value<double>()->default_value("200"))
    ("s,downsampling-factor", "The factor to be used to downsample the input image for upsampling algorithms.", cxxopts::value<double>()->default_value("4.0"))
    ("t,noise-standard-deviation", "Standard deviation to be used as additive noise on the input image for filtering.", cxxopts::value<uchar>()->default_value("25"))
    ("p,point-cloud", "Generate colored point cloud in binary .ply format.", cxxopts::value<bool>()->default_value("true"))
    ("n,nogui", "Do not show result in GUI window")
    ("h,help", "Print usage");

  options.positional_help("<b|bm|jb|jbm|jbu|jbmu|ijbmu> <image> <output_prefix>");
  options.parse_positional({"filter", "image", "output"});

  auto parsed_args = options.parse(argc, argv);
  if (parsed_args.count("help")) {
    std::cout << options.help() << std::endl;
    exit(0);
  }

  // check for required arguments
  if (!parsed_args.count("image") || !parsed_args.count("output") || !parsed_args.count("filter")) {
    std::cout << options.help() << std::endl;
    return EXIT_FAILURE;
  }

  // read the input image (as grayscale)
  cv::Mat ground_truth_image = cv::imread(parsed_args["image"].as<std::string>(), cv::IMREAD_GRAYSCALE);
  if (!ground_truth_image.data) {
    std::cerr << "No image data" << std::endl;
    return EXIT_FAILURE;
  }

  // parse filter type parameter
  std::string filter_string = parsed_args["filter"].as<std::string>();
  Filter filter_type;
  if (!parse_filter(filter_string, filter_type)) {
    std::cerr << "Invalid filter name (possible values: " + possible_filter_names + " ): "
              << filter_string << std::endl;
    return EXIT_FAILURE;
  }
  

  // check whether guiding image is present in case of guided filters
  // if so, load it
  bool is_guided = is_guided_filter(filter_type);  
  cv::Mat guiding_image;
  cv::Mat guiding_color_image;

  if (parsed_args.count("guiding-image")) {
    guiding_color_image = cv::imread(parsed_args["guiding-image"].as<std::string>(), cv::IMREAD_COLOR);
    cv:cvtColor(guiding_color_image, guiding_image, cv::COLOR_BGR2GRAY);
    if (!guiding_image.data) {
      std::cerr << "No guiding image data" << std::endl;
      return EXIT_FAILURE;
    }

    std::cout << "guiding image size: " << guiding_image.size() << std::endl;
  } else if (is_guided) {
    if (!parsed_args.count("guiding-image")) {
      std::cerr << "Missing --guiding_image parameter for joint filtering." << std::endl;
      return EXIT_FAILURE;
    }
  }
  
  const std::string output_file_prefix = parsed_args["output"].as<std::string>();
  
  // parse the parameters
  const int window_size = parsed_args["windowsize"].as<int>();
  const double gaussian_k = parsed_args["gaussian-k"].as<double>();
  const double range_sigma = parsed_args["range-sigma"].as<double>();

  // parameters for 3D reconstruction from disparity maps
  const double focal_length = parsed_args["focallength"].as<double>(); // pixels
  const double baseline = parsed_args["baseline"].as<double>();        // mm
  const double dmin = parsed_args["dmin"].as<double>();

  // parameters for noise and downsampling
  const double downsampling_factor = parsed_args["downsampling-factor"].as<double>();
  const uchar noise_standard_deviation = parsed_args["noise-standard-deviation"].as<uchar>();

  // gui
  const bool no_gui = parsed_args["nogui"].as<bool>();
  const bool gui = !no_gui;

  // point clound
  const bool generate_point_cloud = parsed_args["point-cloud"].as<bool>();
  
  // check window size
  if (window_size % 2 == 0) {
    std::cerr << "Window size should be odd: " << window_size << std::endl;
    return EXIT_FAILURE;
  }

  // check whether it is upsamling or filtering, do the required obfuscation
  bool is_upsampling = is_upsampling_filter(filter_type);

  cv::Mat noisy_image;
  cv::Mat downsampled_image;

  // save difference image
  std::string obfuscated_filename = output_file_prefix + "_obfuscated.png";
  
  if (is_upsampling) {
    // downsample, determine downsampled size with integer division
    const auto downsampled_size = cv::Size2i(ground_truth_image.size().width / downsampling_factor,
                                             ground_truth_image.size().height / downsampling_factor);

    std::cout << "current size: " << ground_truth_image.size() << std::endl;
    std::cout << "downsampled size: " << downsampled_size << " using downsampling factor: " << downsampling_factor << std::endl; 

    cv::resize(ground_truth_image, downsampled_image, downsampled_size, 0, 0, cv::INTER_LINEAR);
    cv::imwrite(obfuscated_filename, downsampled_image);
  } else {
    // create image with noise
    ground_truth_image.copyTo(noisy_image);

    std::cout << "adding noise using standard deviation: " << static_cast<int>(noise_standard_deviation) << std::endl;
    
    // add the noise
    add_image_noise(noisy_image, noise_standard_deviation);
    cv::imwrite(obfuscated_filename, noisy_image);
  }
  
  std::cout << "------------------ Parameters -------------------" << std::endl;
  std::cout << "filter = " << filter_string << std::endl;
  std::cout << "window_size = " << window_size << std::endl;
  std::cout << "gaussian_k = " << gaussian_k << std::endl;
  std::cout << "gaussian_sigma = " << calculate_gaussian_sigma_from_k(window_size, gaussian_k) << std::endl;
  std::cout << "range_sigma = " << range_sigma << std::endl;
  std::cout << "focal_length = " << focal_length << std::endl;
  std::cout << "baseline = " << baseline << std::endl;
  std::cout << "dmin = " << dmin << std::endl;
  std::cout << "-------------------------------------------------" << std::endl;

  // create the exponential range kernel for bilateral filtering
  double range_kernel[256];
  create_exponential_bilateral_range_kernel(window_size, range_sigma, range_kernel);

  // create the gaussian kernel
  cv::Mat gaussian_kernel;
  create_gaussian_kernel(window_size, gaussian_k, gaussian_kernel);
  std::cout << gaussian_kernel << std::endl;

  cv::Mat output;
  // execute the selected algorithm
  int64 start_time = cv::getTickCount();

  switch (filter_type) {
  case Filter::b:
    bilateral_convolution(noisy_image, window_size, gaussian_kernel, range_kernel, output);
    break;
  case Filter::jb:
    joint_bilateral_convolution(noisy_image, guiding_image, window_size, gaussian_kernel, range_kernel, output);
    break;
  case Filter::jbu:
    joint_bilateral_upsampling_convolution(downsampled_image, guiding_image, window_size, gaussian_kernel, range_kernel, output);
    break;
  case Filter::ijbu:
    iterative_joint_bilateral_upsampling_convolution(downsampled_image, guiding_image, window_size, gaussian_kernel, range_kernel, output);
    break;
  default:
    std::cerr << "Not implemented filter: " << filter_string << std::endl;
    return EXIT_FAILURE;
  }
  double elapsed_time =
      ((double)(cv::getTickCount() - start_time)) / cv::getTickFrequency();
  std::cout << "Elapsed time (s): " << elapsed_time << std::endl;

  // save the output image
  std::string output_filename = output_file_prefix + "_output.png";
  cv::imwrite(output_filename, output);

  // evaluate and save stat file
  std::cout << "Evaluating using original ground truth image..." << std::endl;

  cv::Mat difference_image;
  std::string stat_filename = output_file_prefix + "_stat.csv";
  evaluate(output, ground_truth_image, difference_image, stat_filename,
           elapsed_time);

  // save difference image
  std::string difference_filename = output_file_prefix + "_difference.png";
  cv::imwrite(difference_filename, difference_image);

  std::cout << "Difference image has written to " << difference_filename << std::endl;
  std::cout << "Stats has written to " << stat_filename << std::endl;

  if (generate_point_cloud) {
    // 3d reconstruction from disparity map (we assume we work with disparity maps)
    disparity_map_to_point_cloud(output, guiding_color_image, window_size, baseline, focal_length, dmin, output_file_prefix);
  }
  
  if (gui) {
    cv::namedWindow("Original Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Original Image", ground_truth_image);

    if (is_upsampling) {
      cv::namedWindow("Downsampled Image", cv::WINDOW_NORMAL);
      cv::resizeWindow("Downsampled Image", ground_truth_image.size());
      cv::imshow("Downsampled Image", downsampled_image);
    } else {
      cv::namedWindow("Noisy Image", cv::WINDOW_AUTOSIZE);
      cv::imshow("Noisy Image", noisy_image);
    }

    const std::string title = "Result (" + filter_string + ")";
    cv::namedWindow(title, cv::WINDOW_AUTOSIZE);
    cv::imshow(title, output);

    const std::string difference_title = "Difference (" + filter_string + ")";
    cv::namedWindow(difference_title, cv::WINDOW_AUTOSIZE);
    cv::imshow(difference_title, difference_image);
    
    cv::waitKey(0);
  }

}
